Build & Run instructions:

```shell script
cargo run --release -- <arguments>
```

The program tests my Cloudflare General Assignment /links page by default:

```shell script
cargo run --release
```

![Default Endpoint](imgs/result-1.png)

To specify an URL, use --url

```shell script
cargo run --release -- --url https://services.cytoid.io/levels
```

The script prints everything it receives, including the HTTP headers.

To print ONLY the response body, use -t.

```shell script
cargo run --release -- -t
```

![Header Truncated](imgs/result-2.png)


To run profiling, specify the -p argument.

```shell script
cargo run --release -- -p 10
```


![Profile](imgs/profile-worker.png)

Dependencies:

- getopts (For command line argument parsing)
- url (For URL parsing)
- rustls (For making HTTPS requests)
- webpki-roots (Root certificates)
- webpki (Required by rustls)
