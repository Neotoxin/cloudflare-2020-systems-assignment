mod requests;
mod dummy_writer;
use requests::request;
use getopts::Options;
use std::time::Instant;

fn main() {
    let mut args = std::env::args();
    let program_path = args.next().expect("Can't get the program name");
    let mut opts = Options::new();
    opts.optopt("u", "url", "The URL endpoint of the request", "URL");
    opts.optopt("p", "profile", "Enable profile mode and specify the number of requests to make", "COUNT");
    opts.optflag("h", "help", "Print help options");
    opts.optflag("t", "truncate-headers", "Print only the body of the response, not the headers");

    let matches = opts.parse(args);

    if matches.is_err() {
        let usage = opts.usage(&format!("Usage: {} [options]", program_path));
        println!("{}", usage);
        return;
    }
    let matches = matches.unwrap();

    if matches.opt_present("help") {
        let usage = opts.usage(&format!("Usage: {} [options]", program_path));
        println!("{}", usage);
        return;
    }

    let url = matches.opt_str("url").unwrap_or("https://cloudflare-assignment.zhixing.workers.dev/links".to_string());
    let url = url::Url::parse(&url).expect("Invalid URL Format");

    let truncate_headers = matches.opt_present("truncate-headers");

    match matches.opt_str("profile") {
        Some(profile) => {
            let count: usize = profile.parse().expect("Invalid request count. Needs an integer.");

            let mut dummy_stdout = dummy_writer::DummyWriter;
            let responses: Vec<(u64, u16, u128)> = (0..count).map(|i| {
                let now = Instant::now();
                match request(&url, &mut dummy_stdout, truncate_headers) {
                    Ok((byte_num, return_code)) => {
                        let time = now.elapsed().as_millis();
                        println!("Request {} succeeded with {}: received {} bytes in {}ms", i, return_code, byte_num, time);
                        (byte_num, return_code, time)
                    },
                    Err(e) => {
                        let time = now.elapsed().as_millis();
                        println!("Request {} failed in {}ms: {}", i, time, e);
                        (0, 0, time)
                    }
                }
            })
                .collect();

            println!("------------------");


            let num_failed = responses.iter().filter(|(_, code, _)| *code == 0).count();
            let num_non200 = responses.iter().filter(|(_, code, _)| *code < 200 || *code >= 400).count();

            println!("Sent {} requests. {} ({}%) requests succeeded; {} requests failed; {} requests returned non-200s status codes.",
                     count,
                    count - num_failed - num_non200,
                    100.0 * (count - num_failed - num_non200) as f64 / count as f64,
                    num_failed,
                    num_non200
            );

            let mut times: Vec<u128> = responses.iter().map(|(_, _, time)| *time).collect();
            times.sort();

            println!("Min time: {}ms, Max time: {}ms, Mean time: {}ms, Median Time: {}ms",
                     times.first().map(|a| *a).unwrap_or(0),
                     times.last().map(|a| *a).unwrap_or(0),
                    times.iter().sum::<u128>() as f64 / count as f64,
                    if count == 1 { times[0]}
                     else if times.len() % 2 == 1 { times[count / 2] }
                     else { times[count / 2] + times[count / 2 + 1] });

            println!("Min response size: {} bytes, max response size: {} bytes",
                     responses.iter().map(|(size, _, _)| *size).min().unwrap_or(0),
                     responses.iter().map(|(size, _, _)| *size).max().unwrap_or(0));
        },
        None => {
            let mut stdout = std::io::stdout();
            request(&url, &mut stdout, truncate_headers).expect("Error while making the request");
        }
    }
}
