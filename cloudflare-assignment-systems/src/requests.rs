use std::net::TcpStream;
use std::io::{self, Write, Read};
use std::sync::Arc;

pub const DEFAULT_BUF_SIZE: usize = 8 * 1024;

/// Abandon all HTTP header characters and copy ONLY the
/// HTTP body characters to writer.
pub fn copy_response_body<R: ?Sized, W: ?Sized>(reader: &mut R, writer: &mut W, truncate_headers: bool) -> io::Result<(u64, u16)>
    where
        R: Read,
        W: Write,
{
    // A modified std::io::copy
    // A state machine. Each \r or \n increments it. Any other character decrements this value.
    let mut state: u8 = 0;
    let mut status_code: Option<u16> = None;
    let mut status_code_ended = false;

    let mut buf: [u8; DEFAULT_BUF_SIZE] = [0; DEFAULT_BUF_SIZE];

    let mut written = 0;
    loop {
        let len = match reader.read(&mut buf) {
            Ok(0) => return Ok((written, status_code.unwrap_or(0))),
            Ok(len) => len,
            Err(ref e) if e.kind() == io::ErrorKind::Interrupted => continue,
            Err(e) => return Err(e),
        };

        for c in &buf {
            let c = *c;
            if c == b' ' && status_code.is_none() {
                status_code = Some(0)
            } else if c == b' ' && !status_code_ended {
                status_code_ended = true;
            } else if status_code.is_some() && !status_code_ended {
                status_code = Some(status_code.unwrap() * 10 + c as u16 - 48);
            }
        }

        // Recognize a \r\n\r\n sequence and the status code field
        if truncate_headers && state != 4 {
            for (index, c) in buf.iter().enumerate() {
                let c = *c;

                if state == 0 || state == 2 {
                    if c == b'\r' {
                        state += 1;
                    } else {
                        state = 0;
                    }
                } else if state == 1 || state == 3 {
                    if c == b'\n' {
                        state += 1;
                    } else {
                        state = 0;
                    }
                }
                if state == 4 {
                    writer.write(&buf[index+1..])?;
                    break;
                }
            }
        } else {
            writer.write_all(&buf)?;
        }
        written += len as u64;
    }
}

fn make_request<T: Write + Read, OUT: Write>(socket: &mut T, url: &url::Url, stdout: &mut OUT, truncate_headers: bool) -> io::Result<(u64, u16)> {
    socket.write(format!("GET {} HTTP/1.1\r\n\
    Host: {}\r\n\
    Accept-Encoding: identity\r\n\
    Connection: close\r\n\r\n", url.path(), url.host_str().unwrap()).as_bytes())?;

    let result = copy_response_body(socket, stdout, truncate_headers)?;
    stdout.write(b"\n")?;
    Ok(result)
}

fn make_https_request<OUT: Write>(url: &url::Url, stdout: &mut OUT, truncate_headers: bool) -> io::Result<(u64, u16)> {
    let mut config = rustls::ClientConfig::new();
    config.root_store.add_server_trust_anchors(&webpki_roots::TLS_SERVER_ROOTS);
    let config = Arc::new(config);


    let tls_hostname = webpki::DNSNameRef::try_from_ascii_str(url.host_str().expect("Can't parse the hostname")).unwrap();
    let mut tls_session = rustls::ClientSession::new(&config, tls_hostname);

    let socket_addres = url.socket_addrs(|| None).expect("Can't parse the socket address");

    let mut socket = TcpStream::connect(&*socket_addres).expect("Can't open the socket");
    let mut tls_socket = rustls::Stream::new(&mut tls_session, &mut socket);

    make_request(&mut tls_socket, &url, stdout, truncate_headers)
}

fn make_http_request<OUT: Write>(url: &url::Url, stdout: &mut OUT, truncate_headers: bool) -> io::Result<(u64, u16)> {
    let socket_addres = url.socket_addrs(|| None).expect("Can't parse the socket address");
    let mut socket = TcpStream::connect(&*socket_addres).expect("Can't open the socket");
    make_request(&mut socket, &url, stdout, truncate_headers)
}


pub fn request<OUT: Write>(url: &url::Url, stdout: &mut OUT, truncate_headers: bool) -> io::Result<(u64, u16)> {
    if url.scheme() == "https" {
        make_https_request(&url, stdout, truncate_headers)
    } else if url.scheme() == "http"  {
        make_http_request(&url, stdout, truncate_headers)
    } else {
        panic!("Invalid URL Scheme, needs to be http or https");
    }
}
